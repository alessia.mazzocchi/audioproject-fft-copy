﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using System;
using System.Runtime.InteropServices;

public class BeatManager : MonoBehaviour
{
	public static BeatManager instance;

    [SerializeField]
    [EventRef]
    private string music = null;

	public TimelineInfo timelineInfo = null;
	private GCHandle timelineHandle;

	[HideInInspector]
    public FMOD.Studio.EventInstance musicInstance;

	private FMOD.Studio.EVENT_CALLBACK beatCallback;

	public delegate void BeatEventDelegate();
	public static event BeatEventDelegate beatUpdated;

	public delegate void MarkerListenerDelegate();
	public static event MarkerListenerDelegate markerUpdated;
	
	public static int lastBeat = 0;
	public static string lastMarkerString = null;

	[StructLayout(LayoutKind.Sequential)]
	public class TimelineInfo
	{
		public int currentBeat = 0;
		public FMOD.StringWrapper lastMarker = new FMOD.StringWrapper();
	}

	private void Awake()
	{
		instance = this;

		if (music != null)
		{
			musicInstance = RuntimeManager.CreateInstance(music);
		}
	}

	// Set up of timeline and beat event
	private void Start()
	{
		if (music != null)
		{
			timelineInfo = new TimelineInfo();
			beatCallback = BeatEventCallback;
			timelineHandle = GCHandle.Alloc(timelineInfo, GCHandleType.Pinned);
			musicInstance.setUserData(GCHandle.ToIntPtr(timelineHandle));
			musicInstance.setCallback(beatCallback, FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_BEAT | FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_MARKER);
		}
	}

	// Stop music and clear data on destroy
	private void OnDestroy()
	{
		musicInstance.setUserData(IntPtr.Zero);
		musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		musicInstance.release();
		timelineHandle.Free();
	}

	// Update beat and marker if different from previous ones
	private void Update()
	{
		if(lastMarkerString != timelineInfo.lastMarker)
		{
			lastMarkerString = timelineInfo.lastMarker;

			if(markerUpdated != null)
			{
				markerUpdated();
			}
		}

		if(lastBeat != timelineInfo.currentBeat)
		{
			lastBeat = timelineInfo.currentBeat;

			if(beatUpdated != null)
			{
				beatUpdated();
			}
		}
	}

	// Get FMOD event instance
	public FMOD.Studio.EventInstance GetEventInstance()
	{
		return musicInstance;
	}

	// Beat detection system linked to FMOD
#if UNITY_EDITOR // || DEVELOPMENT_BUILD

	// Show current beat and last marker reached in editor (last marker commented due to no implementatio)
	private void OnGUI()
	{
		GUILayout.Box($"Current Beat = {timelineInfo.currentBeat}"); // , Last Marker = {(string)timelineInfo.lastMarker}
	}
#endif

	[AOT.MonoPInvokeCallback(typeof(FMOD.Studio.EVENT_CALLBACK))]
	static FMOD.RESULT BeatEventCallback(FMOD.Studio.EVENT_CALLBACK_TYPE type, FMOD.Studio.EventInstance instancePtr, IntPtr parameterPtr)
	{
		FMOD.Studio.EventInstance instance = instancePtr;

		IntPtr timelineInfoPtr;
		FMOD.RESULT result = instance.getUserData(out timelineInfoPtr);

		if(result != FMOD.RESULT.OK)
		{
			Debug.LogError("Timeline Callback error: " + result);
		}
		else if (timelineInfoPtr != IntPtr.Zero)
		{
			GCHandle timelineHandle = GCHandle.FromIntPtr(timelineInfoPtr);
			TimelineInfo timelineInfo = (TimelineInfo)timelineHandle.Target;

			switch (type)
			{
				case FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_BEAT:
					{
						var parameter = (FMOD.Studio.TIMELINE_BEAT_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(FMOD.Studio.TIMELINE_BEAT_PROPERTIES));
						timelineInfo.currentBeat = parameter.beat;
					}
					break;

				case FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_MARKER:
					{
						var parameter = (FMOD.Studio.TIMELINE_MARKER_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(FMOD.Studio.TIMELINE_MARKER_PROPERTIES));
						timelineInfo.lastMarker = parameter.name;
					}
					break;
				
				default:
					break;
			}
		}
		return FMOD.RESULT.OK;
	}
}
