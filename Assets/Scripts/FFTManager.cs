﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class FFTManager : MonoBehaviour
{
    private FMOD.Studio.EventInstance musicInstance;
    FMOD.DSP fft;
    const int WindowSize = 1024;
    const float WIDTH = 10.0f;
    const float HEIGHT = 0.1f;

    private float[] _samplesLeft = new float[512];
    private float[] _samplesRight = new float[512];
    public static float[] _freqBand = new float[8];
    public static float[] _bandBuffer = new float[8];
    private float[] _bufferDecrease = new float[8];

    private float[] _freqBandHighest = new float[8];
    public static float[] _audioBand = new float[8];
    public static float[] _audioBandBuffer = new float[8];

    public static float _Amplitude, _AmplitudeBuffer;
    float _AmplitudeHighest = 1;

    public float _audioProfile;

    private float _pitch;
    private float _tempPitch;
    public enum _pitchAxisModifier { x, z, both}
    [Space]
    public _pitchAxisModifier _pitchCurrentAxis = new _pitchAxisModifier();
    public float _MinPitch = 0.5f;
    public float _MaxPitch = 1.5f;

    public enum _channel { Stereo, Left, Right };
    [Space]
    public _channel channel = new _channel();

    public static FFTManager mainFFTManager;

    private void Awake()
    {
        mainFFTManager = this;
    }

    void Start()
    {
        musicInstance = BeatManager.instance.musicInstance;
        AudioProfile(_audioProfile);
        CreateFft();
    }

    // Create and set up Digital Signal Processing (DSP) custom plugin with type FFT and link it to FMOD, then start music
    void CreateFft()
	{
        FMODUnity.RuntimeManager.CoreSystem.createDSPByType(FMOD.DSP_TYPE.FFT, out fft);
        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWTYPE, (int)FMOD.DSP_FFT_WINDOW.HANNING);
        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, WindowSize * 2);

        FMOD.ChannelGroup channelGroup;
        FMODUnity.RuntimeManager.CoreSystem.getMasterChannelGroup(out channelGroup);
        channelGroup.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, fft);
        
        musicInstance.start();
    }

    void Update()
    {
        ManageFft();
        MakeFrequencyBands();
        BandBuffer();
        CreateAudioBands();
        GetAmplitude();
    }

    void AudioProfile(float audioProfile)
    {
        for (int i = 0; i < 8; i++)
        {
            _freqBandHighest[i] = audioProfile;
        }
    }

    // Get amplitude with and without buffer
    void GetAmplitude()
    {
        float _CurrentAmplitude = 0;
        float _CurrentAmplitudeBuffer = 0;

        for (int i = 0; i < 8; i++)
        {
            _CurrentAmplitude += _audioBand[i];
            _CurrentAmplitudeBuffer += _audioBandBuffer[i];
        }

        if (_CurrentAmplitude > _AmplitudeHighest)
        {
            _AmplitudeHighest = _CurrentAmplitude;
        }

        _Amplitude = _CurrentAmplitude / _AmplitudeHighest;
        _AmplitudeBuffer = _CurrentAmplitudeBuffer / _AmplitudeHighest;
    }

	// Set new audio bands value with and without buffer
	void CreateAudioBands()
	{
		for (int i = 0; i < 8; i++)
		{
			_audioBand[i] = (_freqBand[i] / _freqBandHighest[i]);
			_audioBandBuffer[i] = (_bandBuffer[i] / _freqBandHighest[i]);
		}
	}

    // Get FFT data and its spectrum
	void ManageFft()
	{
        System.IntPtr unmanagedData;
        uint length;
        fft.getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out unmanagedData, out length);
        FMOD.DSP_PARAMETER_FFT fftData = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(unmanagedData, typeof(FMOD.DSP_PARAMETER_FFT));
        var spectrum = fftData.spectrum;

        if (spectrum.Length != 0)
		{
            _samplesLeft = spectrum[0];
            _samplesRight = spectrum[1];
		}
	}

    // Set band buffer
    void BandBuffer()
    {
        for (int i = 0; i < 8; i++)
        {
            if (_freqBand[i] > _bandBuffer[i])
            {
                _bandBuffer[i] = _freqBand[i];
                _bufferDecrease[i] = 0.005f;
            }

            if (_freqBand[i] < _bandBuffer[i])
            {
                _bandBuffer[i] -= _bufferDecrease[i];
                _bufferDecrease[i] *= 1.2f;
            }
        }
    }

    // Create frequency bands based on commented sample tab below
    void MakeFrequencyBands()
    {
        // 22050 / 512 = 43 hertz per sample
        // 20 - 60 hertz
        // 60 - 250 hertz
        // 250 - 500 hertz
        // 500 - 2000 hertz
        // 2000 - 4000 hertz
        // 4000 - 6000 hertz
        // 6000 - 20000 hertz

        // 0 - 2 = 86 hertz
        // 1 - 4 = 172 hertz - 87-258
        // 2 - 8 = 244 hertz - 259-602
        // 3 - 16 = 688 hertz - 603-1290
        // 4 - 32 = 1376 hertz - 1291-2666
        // 5 - 64 = 2752 hertz - 2667-5418
        // 6 - 128 = 5504 hertz - 5419-10922
        // 7 - 256 = 11008 hertz - 10923-21930

        // 510

        int count = 0;

        for (int i = 0; i < 8; i++)
        {
            float average = 0;

            int sampleCount = (int)Mathf.Pow(2, i) * 2;

            if (i == 7)
            {
                sampleCount += 2;
            }

            for (int j = 0; j < sampleCount; j++)
            {
                if (channel == _channel.Stereo)
                {
                    average += (_samplesLeft[count] + _samplesRight[count]) * (count + 1);
                }
                if (channel == _channel.Left)
                {
                    average += _samplesLeft[count] * (count + 1);
                }
                if (channel == _channel.Right)
                {
                    average += _samplesRight[count] * (count + 1);
                }
                count++;
            }

            average /= count;

            _freqBand[i] = average * 10;
        }
    }

    // Change audio pitch based on movement
    public void ChangeAudioPitch(Vector3 moveDir)
    {
        musicInstance.getPitch(out _pitch);

		switch (_pitchCurrentAxis)
		{
			case _pitchAxisModifier.x:
                _tempPitch = _pitch + (moveDir.x / 10000);
                break;

			case _pitchAxisModifier.z:
                _tempPitch = _pitch + (moveDir.z / 10000);
                break;

			case _pitchAxisModifier.both:
                _tempPitch = _pitch + ((moveDir.x + moveDir.z) / 100000);
                break;

			default:
                _tempPitch = _pitch + ((moveDir.x + moveDir.z) / 100000);
                break;
		}

        _tempPitch = Mathf.Clamp(_tempPitch, _MinPitch, _MaxPitch);
        musicInstance.setPitch(_tempPitch);
    }

    // Reset pitch to one
    public void ResetPitch()
	{
        musicInstance.setPitch(1);
    }
}
