﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Light))]
public class LightOnAudio : MonoBehaviour
{ 
	public int _band;
    public float _minIntensity, _maxIntensity;

    private Light _light;

    void Start()
    {
        _light = GetComponent<Light>();
    }

    // Set new light intensity based on FFT calculated audio band with buffer
    void Update()
    {
        _light.intensity = (FFTManager._audioBandBuffer[_band] * (_maxIntensity - _minIntensity)) +_minIntensity;
    }
}
