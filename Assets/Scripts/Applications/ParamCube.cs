﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour
{
    public int _band;
    public float _startScale, _scaleMultiplier;

    public bool _useBuffer;
    public bool _stretchOneDirOnly;
    public bool _changeColor;

    private Material _material;

    void Start()
    {
        if (_stretchOneDirOnly)
            _material = GetComponentInChildren<MeshRenderer>().materials[0];
        else
            _material = GetComponent<MeshRenderer>().materials[0];
    }

    void Update()
    {
        // Set new scale based on FFT calculated audio band with buffer
        if (_useBuffer)
		{
            transform.localScale = new Vector3(transform.localScale.x, (FFTManager._audioBandBuffer[_band] * _scaleMultiplier) + _startScale, transform.localScale.z);
            
            if (_changeColor)
			{
				Color _color = new Color(FFTManager._audioBandBuffer[_band], FFTManager._audioBandBuffer[_band], FFTManager._audioBandBuffer[_band]);
				_material.SetColor("_Color", _color);
			}
        }
        // Set new scale based on FFT calculated audio band with no buffer
        else
        {
            transform.localScale = new Vector3(transform.localScale.x, (FFTManager._audioBand[_band] * _scaleMultiplier) + _startScale, transform.localScale.z);
			
            if (_changeColor)
			{
				Color _color = new Color(FFTManager._audioBand[_band], FFTManager._audioBand[_band], FFTManager._audioBand[_band]);
				_material.SetColor("_Color", _color);
			}
		}
	}
}
