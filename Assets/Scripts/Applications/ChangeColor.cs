﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public bool _useBuffer;
    public Color _newColor;

    private Material _material;
    private Color _color;

    void Start()
    {
        _material = GetComponent<MeshRenderer>().materials[0];
    }

    void Update()
    {
        // Set new color based on FFT calculated amplitude with buffer
        if (_useBuffer)
		{
			_color = new Color(_newColor.r * FFTManager._AmplitudeBuffer, _newColor.g * FFTManager._AmplitudeBuffer, _newColor.b * FFTManager._AmplitudeBuffer);
			_material.SetColor("_Color", _color);
		}
        // Set new color based on FFT calculated amplitude with no buffer
        else
        {
			_color = new Color(_newColor.r * FFTManager._Amplitude, _newColor.g * FFTManager._Amplitude, _newColor.b * FFTManager._Amplitude);
			_material.SetColor("_Color", _color);
        }
    }
}
