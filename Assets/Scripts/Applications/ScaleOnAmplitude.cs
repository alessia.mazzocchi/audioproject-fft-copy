﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleOnAmplitude : MonoBehaviour
{
    public float _startScale, _maxScale;
    public bool _useBuffer;
    public bool _changeColor;
    Material _material;
    public Color _newColor;
    [Space]
    public bool scaleX = true;
    public bool scaleY = true;
    public bool scaleZ = true;

	private float initX;
    private float initY;
	private float initZ;

    private Vector3 tempVector = new Vector3();

	void Start()
    {
        _material = GetComponent<MeshRenderer>().materials[0];
        initX = transform.localScale.x;
        initY = transform.localScale.y;
		initZ = transform.localScale.z;
        tempVector = new Vector3(initX, initY, initZ);
	}

    void Update()
    {
		// Set new scale based on FFT calculated amplitude with buffer
		if (_useBuffer)
		{
			SetVectorWithBuffer();
			transform.localScale = tempVector;

			if (_changeColor)
			{
				Color _color = new Color(_newColor.r * FFTManager._AmplitudeBuffer, _newColor.g * FFTManager._AmplitudeBuffer, _newColor.b * FFTManager._AmplitudeBuffer);
				_material.SetColor("_Color", _color);
			}
		}
		// Set new scale based on FFT calculated amplitude with no buffer
		else
		{
            SetVectorNoBuffer();
            transform.localScale = tempVector;

            if (_changeColor)
			{
				Color _color = new Color(_newColor.r * FFTManager._Amplitude, _newColor.g * FFTManager._Amplitude, _newColor.b * FFTManager._Amplitude);
				_material.SetColor("_Color", _color);
			}
		}
    }

	private void SetVectorNoBuffer()
	{
        if (scaleX)
            tempVector.x = (FFTManager._Amplitude * _maxScale) + _startScale;
        if (scaleY)
            tempVector.y = (FFTManager._Amplitude * _maxScale) + _startScale;
        if (scaleZ)
            tempVector.z = (FFTManager._Amplitude * _maxScale) + _startScale;
    }

	private void SetVectorWithBuffer()
	{
		if (scaleX)
			tempVector.x = (FFTManager._AmplitudeBuffer * _maxScale) + _startScale;
		if (scaleY)
			tempVector.y = (FFTManager._AmplitudeBuffer * _maxScale) + _startScale;
		if (scaleZ)
			tempVector.z = (FFTManager._AmplitudeBuffer * _maxScale) + _startScale;
	}
}
