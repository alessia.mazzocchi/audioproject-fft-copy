﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public Transform PlayerToFollow;
    public Vector3 CameraOffset = new Vector3(0, 40, -40);

    private Transform m_ThisTransform;

    void Start()
    {
        m_ThisTransform = GetComponent<Transform>();
    }

    // Follow player with an offset
    void LateUpdate()
    {
        m_ThisTransform.position = PlayerToFollow.position + CameraOffset;
    }
}
