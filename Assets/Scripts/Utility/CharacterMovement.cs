﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class CharacterMovement : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private bool _changePitch;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            _changePitch = !_changePitch;

		if (Input.GetKeyDown(KeyCode.R))
		{
			_changePitch = false;
            FFTManager.mainFFTManager.ResetPitch();
        }

		// Is the controller on the ground?
		if (controller.isGrounded)
        {
            // Feed moveDirection with input.
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            // Multiply it by speed.
            moveDirection *= speed;
            // Jumping
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }

        // Set pitch
        if (_changePitch)
            FFTManager.mainFFTManager.ChangeAudioPitch(moveDirection);

        // Applying gravity to the controller
        moveDirection.y -= gravity * Time.deltaTime;
        // Making the character move
        controller.Move(moveDirection * Time.deltaTime);
    }
}
