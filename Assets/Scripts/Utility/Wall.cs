﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
	// Destroy bullets
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.GetComponent<BasicBullet>())
			Destroy(collision.gameObject);
	}
}
