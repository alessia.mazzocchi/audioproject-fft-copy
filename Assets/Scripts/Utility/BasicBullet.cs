﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBullet : MonoBehaviour
{
    public float speed;

    private bool _dirHasBeenSet;
    private Vector3 _bulletDirection;
    
    void Update()
    {
        // Go forward
        if (_dirHasBeenSet)
            transform.Translate(_bulletDirection * Time.deltaTime * speed);
    }

    // Set bullet direction when spawned (set by spawner)
    public void SetBulletDirection(Vector3 dir)
	{
        _bulletDirection = dir;
        _dirHasBeenSet = true;
    }
}
