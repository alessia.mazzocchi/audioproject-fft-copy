﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnToTheBeat : MonoBehaviour
{
	[SerializeField]
	private GameObject spawnPoint = null;

	[SerializeField]
    private GameObject objectToSpawn = null;

    [SerializeField]
    private int spawnInterval = 5;

	[Tooltip("Choose between 0 and 7. Use -1 if none.")]
	public int bandToFollow = -1;
	public float bandThreshold = 0;

	private int nextSpawn = 0;

    private void Awake()
    {
        nextSpawn = spawnInterval;
        BeatManager.beatUpdated += Spawn;
    }

    private void OnDestroy()
    {
        BeatManager.beatUpdated -= Spawn;
    }

	// Spawn an object based on spawn interval and band selected (method subscribed to the "beatUpdated" event)
	private void Spawn()
	{
		if (nextSpawn > 0)
		{
			nextSpawn--;
		}
		else
		{
			if (bandToFollow == -1)
			{
				GameObject obj = Instantiate(objectToSpawn, spawnPoint.transform.position, Quaternion.identity);
				obj.GetComponent<BasicBullet>().SetBulletDirection(transform.rotation * Vector3.forward);

				nextSpawn = spawnInterval - 1;
			}
			else if (bandToFollow > -1 && FFTManager._audioBand[bandToFollow] > bandThreshold)
			{
				GameObject obj = Instantiate(objectToSpawn, spawnPoint.transform.position, Quaternion.identity);
				obj.GetComponent<BasicBullet>().SetBulletDirection(transform.rotation * Vector3.forward);

				nextSpawn = spawnInterval - 1;
			}
		}
	}
}
